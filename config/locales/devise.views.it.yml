---
it:
  activerecord:
    attributes:
      user:
        confirmation_sent_at: Conferma inviata a
        confirmation_token: Token di conferma
        confirmed_at: Confermato il
        created_at: Creato il
        current_password: Password corrente
        current_sign_in_at: Accesso corrente il
        current_sign_in_ip: IP accesso corrente
        email: Email
        encrypted_password: Password criptata
        failed_attempts: Tentativi falliti
        last_sign_in_at: Ultimo accesso il
        last_sign_in_ip: Ultimo IP di accesso
        locked_at: Bloccato il
        otp_attempt: OTP
        otp_attempt_hint_html: Codice necessario se hai abilitato l'autenticazione a due fattori (2FA).<br/>Se non riesci ad utilizzare la tua app, puoi utilizzare uno dei tuoi codici OTP di backup.
        password: Password
        password_confirmation: Conferma password
        remember_created_at: Ricordami creato il
        remember_me: Ricordami
        reset_password_sent_at: Reset password inviata a
        reset_password_token: Token di reset password
        sign_in_count: Numero di accessi
        unconfirmed_email: Email non confermata
        unlock_token: Token di sblocco
        updated_at: Aggiornato il
    models:
      user:
        one: Utente
        other: Utenti
  devise:
    confirmations:
      confirmed: Il tuo account è stato correttamente confermato.
      new:
        resend_confirmation_instructions: Invia di nuovo le istruzioni per la conferma
      send_instructions: Riceverai un messaggio email con le istruzioni per confermare il tuo account entro qualche minuto.
      send_paranoid_instructions: Se la tua e-mail esiste nel nostro database, riceverai un messaggio email con le istruzioni per confermare il tuo account entro qualche minuto.
      show:
        account_activation: Conferma dell'account
        activate: Attiva
        choose_a_password: 'Scegli una password:'
        for: per
        password_confirmation: 'Conferma della password:'
    failure:
      already_authenticated: Hai già effettuato l'accesso.
      inactive: Il tuo account non è stato ancora attivato.
      invalid: Credenziali %{authentication_keys} o password non valide.
      last_attempt: Hai ancora un tentativo prima che l'account venga bloccato.
      locked: Il tuo account è bloccato.
      not_found_in_database: Credenziali %{authentication_keys} o password non valide.
      timeout: Sessione scaduta, accedere nuovamente per continuare.
      unauthenticated: Devi accedere o registrarti per continuare.
      unconfirmed: Devi confermare il tuo account per continuare.
    mailer:
      confirmation_instructions:
        action: Conferma il mio account
        greeting: Benvenuto %{recipient}!
        instruction: 'Puoi confermare il tuo account cliccando sul link qui sotto:'
        subject: "[RIA_COVID] Istruzioni per la conferma"
      email_changed:
        greeting: Ciao %{recipient}!
        message: Ti stiamo contattando per notificarti che la tua email è stata modificata in %{email}.
        subject: Email modificata
      password_change:
        greeting: Ciao %{recipient}!
        message: Ti stiamo contattando per notificarti che la tua password è stata modificata.
        subject: Password modificata
      presentations: Ricevi questo messaggio dal sito EPICLIN.
      reset_password_instructions:
        action: Cambia la mia password
        greeting: Ciao %{recipient}!
        instruction: Qualcuno ha richiesto di resettare la tua password, se lo vuoi davvero puoi farlo cliccando sul link qui sotto.
        instruction_2: Se non sei stato tu ad effettuare questa richiesta puoi ignorare questa mail.
        instruction_3: La tua password non cambierà finchè non accederai al link sopra.
        subject: "[RIA_COVID] Istruzioni per reimpostare la password"
      tips:
        plaintext_url: 'Se il tuo programma di posta elettronica non interpreta correttamente il link qui sopra, copia e incolla nella barra degli indirizzi del tuo browser la seguente URL:'
      unlock_instructions:
        action: Sblocca il mio account
        greeting: Ciao %{recipient}!
        instruction: 'Clicca sul link qui sotto per sbloccare il tuo account:'
        message: Il tuo account è stato bloccato a seguito di un numero eccessivo di tentativi di accesso falliti.
        subject: "[RIA_COVID] Istruzioni per sbloccare l'account"
    omniauth_callbacks:
      failure: Non è stato possibile autorizzarti da %{kind} perchè "%{reason}".
      no_matching: Nessun utente corrisponde a %{auth}.
      success: Autorizzato con successo dall'account %{kind}.
    passwords:
      edit:
        change_my_password: Cambia la mia password
        change_your_password: Cambia la tua password
        confirm_new_password: Conferma la nuova password
        new_password: Nuova password
      new:
        forgot_your_password: Password dimenticata?
        send_me_reset_password_instructions: Inviami le istruzioni per resettare la password
      no_token: Puoi accedere a questa pagina solamente dalla email di reset della password. Se vieni dalla email controlla di aver inserito l'url completo riportato nella email.
      send_instructions: Riceverai un messaggio email con le istruzioni per reimpostare la tua password entro qualche minuto.
      send_paranoid_instructions: Se la tua e-mail esiste nel nostro database, riceverai un messaggio email contentente un link per il ripristino della password
      updated: La tua password è stata cambiata. Ora sei collegato.
      updated_not_active: La tua password è stata cambiata.
    registrations:
      destroyed: Arrivederci! L'account è stato cancellato. Speriamo di rivederci presto.
      edit:
        are_you_sure: Sei sicuro?
        cancel_my_account: Rimuovi il mio account
        confirm_your_changes: Conferma i cambiamenti
        contact_information: Informazioni di contatto
        currently_waiting_confirmation_for_email: 'In attesa di conferma per: %{email}'
        leave_blank_if_you_don_t_want_to_change_it: lascia in bianco se non vuoi cambiarla
        login_information: Informazioni di accesso
        personal_details: Dati personali
        title: Modifica %{resource}
        unhappy: Scontento
        update: Aggiorna
        we_need_your_current_password_to_confirm_your_changes: abbiamo bisogno della tua password attuale per confermare i cambiamenti
      new:
        already_registered: Se hai già un utente EPICLIN e hai bisogno di partecipare ad un nuovo studio non serve una nuova registrazione ma basterà seguire le istruzioni ad inizio pagina
        sign_up: Registrati
        sign_up_instructions_1_html: 'Inserisci i tuoi dati e invia il modulo per creare un nuovo utente: riceverai un''email all''indirizzo specificato con le istruzioni per la conferma.'
        sign_up_instructions_2_html: "<p>Per poter operare sui dati dei pazienti, è necessaria l'autorizzazione dei coordinatori degli studi.</p><p><strong>Dopo esserti registrato ed aver confermato il tuo nuovo utente seguendo il link che ti sarà inviato via email, dovrai contattare le persone responsabili o le segreterie di riferimento che troverai nella pagina dei contatti dello studio di tuo interesse, comunicando l'indirizzo email con cui ti sei registrato su EPICLIN.</strong></p><p>Gli amministratori del sito provvederanno successivamente all'assegnazione dei tuoi permessi, al termine della quale riceverai una notifica di attivazione del tuo account via email.</p>"
      signed_up: Iscrizione eseguita correttamente. Se abilitata, una conferma è stata inviata al tuo indirizzo email.
      signed_up_but_inactive: Iscrizione eseguita correttamente. Però non puoi accedere in quanto il tuo account non è ancora attivo.
      signed_up_but_locked: Iscrizione eseguita correttamente. Però non puoi accedere in quanto il tuo account è bloccato.
      signed_up_but_unconfirmed: Ti è stato inviato un messaggio con un link di conferma. Ti invitiamo a visitare il link per attivare il tuo account.
      update_needs_confirmation: Il tuo account è stato aggiornato, ma dobbiamo verificare la tua email. Ti invitiamo a consultare la tua email e cliccare sul link di conferma.
      updated: Il tuo account è stato aggiornato.
    sessions:
      already_signed_out: Sei uscito correttamente.
      new:
        login_classic: Accesso classico
        login_oauth: Accesso con OmniAuth
        login_openid: Accesso con OpenID
        not_yet_registered: Non sei ancora registrato?
        sign_in: Accedi
        sign_in_with: 'Accedi con:'
        sign_in_with_facebook: Accedi con Facebook
        sign_in_with_google: Accedi con Google
        sign_in_with_openid: Accedi con OpenID
        sign_in_with_twitter: Accedi con Twitter
      signed_in: Accesso effettuato con successo.
      signed_out: Sei uscito correttamente.
    shared:
      links:
        back: Indietro
        can_t_sign_in: Non riesci ad accedere?
        didn_t_receive_confirmation_instructions: Non hai ricevuto le istruzioni per la conferma?
        didn_t_receive_unlock_instructions: Non hai ricevuto le istruzioni per lo sblocco?
        forgot_your_password: Password dimenticata?
        sign_in: Accedi
        sign_in_with_provider: Accedi con %{provider}
        sign_out: Esci
        sign_up: Registrati
        your_account: I miei dati
      minimum_password_length:
        one: "(minimo %{count} carattere)"
        other: "(minimo %{count} caratteri)"
      password_hint: La password deve contenere minimo 8 caratteri.
    unlocks:
      new:
        resend_unlock_instructions: Invia di nuovo le istruzioni per lo sblocco
      send_instructions: Riceverai un messaggio email con le istruzioni per sbloccare il tuo account entro qualche minuto.
      send_paranoid_instructions: Se la tua e-mail esiste nel nostro database, riceverai un messaggio email con le istruzioni per sbloccare il tuo account entro qualche minuto.
      unlocked: Il tuo account è stato correttamente sbloccato. Ora sei collegato.
  errors:
    messages:
      already_confirmed: è stato già confermato
      confirmation_period_expired: deve essere confermato entro %{period}, richiedi una nuova conferma
      expired: è scaduto, si prega di richiederne uno nuovo
      not_found: non trovato
      not_locked: non era bloccato
      not_saved:
        one: 'Non posso salvare questo %{resource}: 1 errore'
        other: 'Non posso salvare questo %{resource}: %{count} errori.'
