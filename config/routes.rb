Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: "users/sessions",
    # registrations: "users/registrations",
    confirmations: "users/confirmations", passwords: "users/passwords",
    password_expired: "users/password_expired"}, path: 'account', skip: [:registrations]

  root to: "daily_reports#new"
  resources :daily_reports
  get :downloads, controller: "daily_reports", action: "downloads"
  #resources :changes, :controller => 'paper_trail_manager/changes'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  devise_scope :user do
    patch '/account/confirmation' => 'users/confirmations#update', as: :update_user_confirmation
    get '/account/edit' => 'users/registrations#edit', :as => 'edit_user_registration'
    patch '/account' => 'users/registrations#update', :as => 'user_registration'
  end
end
