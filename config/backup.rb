# encoding: utf-8
##
# Backup v5.x Configuration
# Backup Generated: epiclin2
# Once configured, you can run the backup with the following command:
#
# $ backup perform -t epiclin2 [-c <path_to_configuration_file>]
#
# For more information about Backup's components, see the documentation at:
# http://backup.github.io/backup
#
require 'rubygems'
require 'rails/all'


Model.new(:riacovid_backup, 'Description for riacovid') do

    base_path = "/home/riacovid/deploy/current"


    def encrypted(path, key_path: "config/master.key", env_key: "RAILS_MASTER_KEY")
        ActiveSupport::EncryptedConfiguration.new(
          config_path: path,
          key_path: key_path,
          env_key: env_key,
          raise_if_missing_key: nil
        )
      end

      credentials = YAML.load(encrypted(File.expand_path("#{base_path}/config/credentials.yml.enc"), key_path: File.expand_path("#{base_path}/config/master.key")).read)

    
      ##
      # Local (Copy) [Storage]
      #
    

    
      # store_with SFTP do |server|
      #   # server.username = ENV['NAS_USER']
      #   # server.password = ENV['NAS_PASSWORD']
      #   server.username = credentials['nas_user']
      #   server.password = credentials['nas_password']
      #   server.ip       = '31.197.100.240'
      #   server.port     = 222
      #   server.path     = "/mnt/raid/backups/epiclin_2/backup/#{Time.now.year}/#{Time.now.strftime('%m')}"
      #   server.keep     = 730
      # end
    
      store_with Local do |local|
        local.path = '/home/riacovid/db_backups/'
        local.keep = 10
      end
    
    
      password = credentials["production"]["postgres"]["password"]
    
      database PostgreSQL do |db|
        db.name               = 'ria_covid_production'
        db.username           = 'epiclin'
        db.password           = password
        db.host               =  "localhost"
        db.port               = 5432
        # db.socket             = socket
        #db.skip_tables        = ['skip', 'these', 'tables']
        #db.only_tables        = ['only', 'these' 'tables']
      end
    
    
    
      ##
      # Gzip [Compressor]
      #
      compress_with Gzip
    
      #
      # Mail [Notifier]
      #
      # The default delivery method for Mail Notifiers is 'SMTP'.
      # See the documentation for other delivery options.
    
      notify_by Mail do |mail|
    
        mail.on_success           = true
        mail.on_warning           = true
        mail.on_failure           = true
        mail.from                 = 'info@epiclin.it'
        mail.to                   = 'francesco.brunetti@cpo.it'
        mail.delivery_method      = :smtp
        mail.address              = "smtps.aruba.it"
        mail.port                 = 465
        mail.domain               = 'epiclin.it'
        mail.user_name            = "info@epiclin.it"
        #mail.password             = ENV['SMTP_PASSWORD']
        mail.password             = credentials['production']['smtp']['password']
        mail.encryption = :ssl
      end
    

end
