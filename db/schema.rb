# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_09_01_104327) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "daily_reports", force: :cascade do |t|
    t.bigint "hospital_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "posti_letto_totali"
    t.integer "posti_letto_covid_attivati"
    t.integer "posti_letto_covid_occupati"
    t.integer "posti_letto_covid_disponibili"
    t.integer "occupazione_posti_letto_covid"
    t.integer "ricoveri_covid_24h"
    t.integer "trasferimenti_dimissioni_covid_24h"
    t.integer "decessi_covid_24h"
    t.integer "posti_letto_no_covid_attivati"
    t.integer "posti_letto_no_covid_occupati"
    t.integer "posti_letto_no_covid_disponibili"
    t.integer "occupazione_posti_letto_no_covid"
    t.integer "ricoveri_no_covid_24h"
    t.integer "trasferimenti_dimissioni_no_covid_24h"
    t.integer "decessi_no_covid_24h"
    t.date "report_date"
    t.integer "posti_letto_covid_occupati_vaccinati"
    t.integer "ricoveri_covid_24h_vaccinati"
    t.index ["hospital_id"], name: "index_daily_reports_on_hospital_id"
  end

  create_table "hospitals", force: :cascade do |t|
    t.string "city"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "classificazione"
    t.string "tipo"
    t.string "quadrante"
    t.string "code"
  end

  create_table "hospitals_users", id: false, force: :cascade do |t|
    t.bigint "hospital_id"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["hospital_id"], name: "index_hospitals_users_on_hospital_id"
    t.index ["user_id"], name: "index_hospitals_users_on_user_id"
  end

  create_table "login_activities", force: :cascade do |t|
    t.string "scope"
    t.string "strategy"
    t.string "identity"
    t.boolean "success"
    t.string "failure_reason"
    t.string "user_type"
    t.bigint "user_id"
    t.string "context"
    t.string "ip"
    t.text "user_agent"
    t.text "referrer"
    t.string "city"
    t.string "region"
    t.string "country"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at"
    t.index ["identity"], name: "index_login_activities_on_identity"
    t.index ["ip"], name: "index_login_activities_on_ip"
    t.index ["user_type", "user_id"], name: "index_login_activities_on_user"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.boolean "write_access", default: false
    t.boolean "is_admin", default: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

end
