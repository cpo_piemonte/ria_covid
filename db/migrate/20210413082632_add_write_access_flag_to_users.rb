class AddWriteAccessFlagToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :write_access, :boolean, default: false
    User.all.each {|u| u.update_column(:write_access, true)}
  end
end
