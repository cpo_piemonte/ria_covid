class AddFieldsToHospitals < ActiveRecord::Migration[6.1]
  def change
    add_column :hospitals, :classificazione, :string
    add_column :hospitals, :tipo, :string
    add_column :hospitals, :quadrante, :string
  end
end
