class AddCodeToHospitals < ActiveRecord::Migration[6.1]
  def change
    add_column :hospitals, :code, :string
    CSV.foreach("db/fixtures/codes.csv", :col_sep => ",", :headers => true) do |row|
      h = Hospital.where("CONCAT(city, ' ', name) = '#{row['hospital']}'").take
      if h 
        puts "Setting code for #{h.full_name}"
        h.update_attribute(:code, row['code'])
      end
    end
  end
end
