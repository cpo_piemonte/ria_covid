class AddReportDateToDailyReports < ActiveRecord::Migration[6.1]
  def change
    add_column :daily_reports, :report_date, :date
    DailyReport.all.each do |d|
      d.update_column(:report_date, d.created_at.to_date)
    end
  end
end
