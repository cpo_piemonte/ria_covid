class AddFieldsToDailyReports < ActiveRecord::Migration[6.1]
  def change
    add_column :daily_reports, :posti_letto_totali, :integer
    add_column :daily_reports, :posti_letto_covid_attivati, :integer
    add_column :daily_reports, :posti_letto_covid_occupati, :integer
    add_column :daily_reports, :posti_letto_covid_disponibili, :integer
    add_column :daily_reports, :occupazione_posti_letto_covid, :integer
    add_column :daily_reports, :ricoveri_covid_24h, :integer
    add_column :daily_reports, :trasferimenti_dimissioni_covid_24h, :integer
    add_column :daily_reports, :decessi_covid_24h, :integer
    add_column :daily_reports, :posti_letto_no_covid_attivati, :integer
    add_column :daily_reports, :posti_letto_no_covid_occupati, :integer
    add_column :daily_reports, :posti_letto_no_covid_disponibili, :integer
    add_column :daily_reports, :occupazione_posti_letto_no_covid, :integer
    add_column :daily_reports, :ricoveri_no_covid_24h, :integer
    add_column :daily_reports, :trasferimenti_dimissioni_no_covid_24h, :integer
    add_column :daily_reports, :decessi_no_covid_24h, :integer
  end
end
