class AddOtherUsers < ActiveRecord::Migration[6.1]
  def change
    emails = %w(bedmanager@gradenigo.it clemente.ponzetti@policlinicodimonza.it elide.azzan@asl.novara.it)
    emails.each do |email|
      User.find_or_create_by(email: email)
    end
  end
end
