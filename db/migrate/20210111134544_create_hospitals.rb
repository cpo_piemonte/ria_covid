class CreateHospitals < ActiveRecord::Migration[6.1]
  def change
    create_table :hospitals do |t|
      t.string :city
      t.string :name
      t.timestamps
    end
  end
end
