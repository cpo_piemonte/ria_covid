class AddVaccinatiToDailyReports < ActiveRecord::Migration[6.1]
  def change
    add_column :daily_reports, :posti_letto_covid_occupati_vaccinati, :integer
    add_column :daily_reports, :ricoveri_covid_24h_vaccinati, :integer
  end
end
