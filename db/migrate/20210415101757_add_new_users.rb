class AddNewUsers < ActiveRecord::Migration[6.1]
  def up
    CSV.foreach("db/fixtures/referenti_covid.csv", :col_sep => ";", :headers => true) do |row|
      
      row['email'].scan(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i) do |email|
        #user = User.find_or_create_by(email: email)
        #user.send_confirmation_instructions
      end
    end
  end
  
  def down
    CSV.foreach("db/fixtures/referenti_covid.csv", :col_sep => ";", :headers => true) do |row|
      
      row['email'].scan(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i) do |email|
        user = User.destroy_by(email: email)
      end
    end
  end

end
