class CreateHospitalsUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :hospitals_users, id: false do |t|
      t.belongs_to :hospital
      t.belongs_to :user
      t.timestamps
    end
  end
end
