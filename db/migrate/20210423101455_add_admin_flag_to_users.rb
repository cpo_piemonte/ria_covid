class AddAdminFlagToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :is_admin, :boolean,  default: false
    u = User.where(email: 'francesco.brunetti@cpo.it').take
    u.update(is_admin: true)
  end
end
