class AddUsers < ActiveRecord::Migration[6.1]
  def up
    add_column :users, :confirmation_token, :string
    add_column :users, :confirmed_at, :datetime
    add_column :users, :confirmation_sent_at, :datetime
    # add_column :users, :unconfirmed_email, :string # Only if using reconfirmable
    add_index :users, :confirmation_token, unique: true
    # User.reset_column_information # Need for some types of updates, but not for update_all.
    # To avoid a short time window between running the migration and updating all existing
    # users as confirmed, do the following
    User.update_all confirmed_at: DateTime.now
    admin = User.where(email: "riacovid@epiclin.it").take
    me = User.create(email: 'francesco.brunetti@cpo.it')
    me.send_confirmation_instructions
    CSV.foreach("db/fixtures/users_hospitals.csv", :col_sep => ",", :headers => true) do |row|
      user = User.find_or_create_by(email: row['mail'])
      user.send_confirmation_instructions
      hospital =  Hospital.where(name: row['name']).take
      if hospital
        hospital.users << user 
        hospital.users << admin
        hospital.users << me
      else
        puts "#{row['name']} not found in DB!!"
      end
    end
  end

  def down
    remove_column :users, :confirmation_token
    remove_column :users, :confirmed_at
    remove_column :users, :confirmation_sent_at
    #remove_index :users, :confirmation_token
  end
end