# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#  movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#  Character.create(name: 'Luke', movie: movies.first)
require 'csv'
User.destroy_all
User.create(email: 'riacovid@epiclin.it', password: 'nosgecoe')
Hospital.destroy_all
CSV.foreach("db/fixtures/ospedali.csv", :col_sep => ",", :headers => true) do |row|
  Hospital.create(name: row['nome'], code: row['code'],classificazione: row['classificazione'], tipo: row['tipo'], quadrante: row['quadrante'])
end
