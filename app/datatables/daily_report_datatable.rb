class DailyReportDatatable < Effective::Datatable
  
  datatable do
    
    actions_col(only: [:edit]) if current_user.write_access
    col :created_at, search: false, label: "Data" do |r|
      I18n.l(r.created_at.to_date)
    end
    col 'hospital.full_name', label: 'Presidio', search: false
    col :posti_letto_totali, search: false, label: I18n.t("activerecord.attributes.daily_report.posti_letto_totali")
    col :posti_letto_covid_attivati, search: false, label: I18n.t("activerecord.attributes.daily_report.posti_letto_covid_attivati")
    col :posti_letto_covid_occupati, search: false, label: I18n.t("activerecord.attributes.daily_report.posti_letto_covid_occupati")
    col :posti_letto_covid_occupati_vaccinati, search: false, label: I18n.t("activerecord.attributes.daily_report.posti_letto_covid_occupati_vaccinati")
    col :posti_letto_covid_disponibili, search: false, label: I18n.t("activerecord.attributes.daily_report.posti_letto_covid_disponibili")
    col :occupazione_posti_letto_covid, search: false, label: I18n.t("activerecord.attributes.daily_report.occupazione_posti_letto_covid")
    col :ricoveri_covid_24h, search: false, label: I18n.t("activerecord.attributes.daily_report.ricoveri_covid_24h")
    col :ricoveri_covid_24h_vaccinati, search: false, label: I18n.t("activerecord.attributes.daily_report.ricoveri_covid_24h_vaccinati")
    col :trasferimenti_dimissioni_covid_24h, search: false, label: I18n.t("activerecord.attributes.daily_report.trasferimenti_dimissioni_covid_24h")
    col :decessi_covid_24h, search: false, label: I18n.t("activerecord.attributes.daily_report.decessi_covid_24h")
    col :posti_letto_no_covid_attivati, search: false, label: I18n.t("activerecord.attributes.daily_report.posti_letto_no_covid_attivati")
    col :posti_letto_no_covid_occupati, search: false, label: I18n.t("activerecord.attributes.daily_report.posti_letto_no_covid_occupati")
    col :posti_letto_no_covid_disponibili, search: false, label: I18n.t("activerecord.attributes.daily_report.posti_letto_no_covid_disponibili")
    col :occupazione_posti_letto_no_covid, search: false, label: I18n.t("activerecord.attributes.daily_report.occupazione_posti_letto_no_covid")
    col :ricoveri_no_covid_24h, search: false, label: I18n.t("activerecord.attributes.daily_report.ricoveri_no_covid_24h")
    col :trasferimenti_dimissioni_no_covid_24h, search: false, label: I18n.t("activerecord.attributes.daily_report.trasferimenti_dimissioni_no_covid_24h")
    col :decessi_no_covid_24h, search: false, label: I18n.t("activerecord.attributes.daily_report.decessi_no_covid_24h")
    length 50
    order :created_at ,:desc
   
    col :updated_at, search: false
    
  end

  collection do
    scope = DailyReport.joins(:hospital).all
    if filters[:start_date].present?
      scope = scope.where('daily_reports.created_at >= ?', filters[:start_date].to_datetime.beginning_of_day)
    end
    if filters[:end_date].present?
      scope = scope.where('daily_reports.created_at <= ?', filters[:end_date].to_datetime.end_of_day)
    end
    if filters[:hospital].present?
      scope = scope.where(hospital: filters[:hospital])
    end
    scope
  end

  filters do
    filter :start_date, nil, required: false, as: :date#, label: 'Data inizio'
    filter :end_date, nil, required: false, as: :date#, label: 'Data fine'
    filter :hospital, nil, collection: Hospital.all.order("city, name").map{|h| [h.name, h.id]}, prompt: 'Presidio', multiple: false#, label: 'Presidio'
  end

end