class DailyReport < ApplicationRecord
  has_paper_trail
  belongs_to :hospital
  acts_as_xlsx :columns => ["hospital.full_name", :date,:posti_letto_totali, :posti_letto_covid_attivati, :posti_letto_covid_occupati, :posti_letto_covid_occupati_vaccinati, :posti_letto_covid_disponibili, :occupazione_posti_letto_covid, :ricoveri_covid_24h, :ricoveri_covid_24h_vaccinati, :trasferimenti_dimissioni_covid_24h, :decessi_covid_24h, :posti_letto_no_covid_attivati, :posti_letto_no_covid_occupati, :posti_letto_no_covid_disponibili, :occupazione_posti_letto_no_covid, :ricoveri_no_covid_24h, :trasferimenti_dimissioni_no_covid_24h, :decessi_no_covid_24h ], :i18n => 'activerecord.attributes'
  scope :today, -> { where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day) }
  #scope :weekly, -> {where(updated_at: DateTime.now.beginning_of_week..DateTime.now.end_of_week)}

  before_save :set_calculated_values

  validates :posti_letto_totali,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  validates :posti_letto_covid_attivati,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  validates :posti_letto_covid_occupati,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  #validates :posti_letto_covid_disponibili,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  #validates :occupazione_posti_letto_covid,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  validates :ricoveri_covid_24h,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  validates :trasferimenti_dimissioni_covid_24h,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  validates :decessi_covid_24h,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  validates :posti_letto_no_covid_attivati,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  validates :posti_letto_no_covid_occupati,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  #validates :posti_letto_no_covid_disponibili,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  #validates :occupazione_posti_letto_no_covid,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  validates :ricoveri_no_covid_24h,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  validates :trasferimenti_dimissioni_no_covid_24h,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }
  validates :decessi_no_covid_24h,  numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_nil: true }

  validates_uniqueness_of :hospital_id, scope: :report_date, message: "Report già presente per l'ospedale e la data selezionati"
  validates_uniqueness_of :report_date, scope: :hospital_id, message: "Report già presente per l'ospedale e la data selezionati"

  def set_calculated_values
    self.posti_letto_totali = calculate_posti_letto_totali
    self.posti_letto_covid_disponibili = calculate_posti_letto_covid_disponibili
    self.posti_letto_no_covid_disponibili = calculate_posti_letto_no_covid_disponibili
    self.occupazione_posti_letto_covid = calculate_occupazione_posti_letto_covid
    self.occupazione_posti_letto_no_covid = calculate_occupazione_posti_letto_no_covid
  end

  def date
    I18n.l(report_date.to_date)
  end

  def calculate_posti_letto_totali
    posti_letto_covid_attivati.to_i + posti_letto_no_covid_attivati.to_i
  end

  def calculate_posti_letto_covid_disponibili
    posti_letto_covid_attivati.to_i - posti_letto_covid_occupati.to_i
  end

  def calculate_posti_letto_no_covid_disponibili
    posti_letto_no_covid_attivati.to_i - posti_letto_no_covid_occupati.to_i
  end

  def calculate_occupazione_posti_letto_covid
    indice = (posti_letto_covid_occupati.to_f / posti_letto_covid_attivati.to_f) * 100 rescue nil
    if indice.nan?
      return 0
    else 
      return indice.round(1)
    end
  end

  def calculate_occupazione_posti_letto_no_covid
    indice = (posti_letto_no_covid_occupati.to_f / posti_letto_no_covid_attivati.to_f) * 100 rescue nil
    if indice.nan?
      return 0
    else 
      return indice.round(1)
    end
  end
  
end
