class Hospital < ApplicationRecord
  has_many :daily_reports
  has_and_belongs_to_many :users

  default_scope { where("code is not null and code != ''") }

  TSI_COVID_ATTIVE = 14
  TSI_NO_COVID_ATTIVE = 15
  
  def full_name
    "#{name}"
  end
end
