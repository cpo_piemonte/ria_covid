class DailyReportsController < ApplicationController
  before_action :set_daily_report, only: [:show, :edit, :update, :destroy]

  # GET /daily_reports
  # GET /daily_reports.json

  def downloads
    Groupdate.week_start = :monday
    respond_to do |format|
      format.xlsx {
        case params[:filter]
        when 'all'
          xlsx_package = DailyReport.joins(:hospital).order(:code, :report_date).to_xlsx
          file_name = "total_#{Time.now.to_date}"
        when 'daily'
          report_date = params[:report_date]
          file_name = "daily_#{report_date.to_date}"
          data = DailyReport.where(report_date: report_date).joins(:hospital).order(:code)
          xlsx_package = data.to_xlsx
          if params[:with_summary] == 'true'
            ws = xlsx_package.workbook.worksheets.first
            ws.add_row([""])
            ws.add_row(["","SOMMARIO"])
            ws.add_row([""])
            strutture_attive = Hospital.all.count
            rispondenza = {}
            percentuale_rispondenza = {}
            somme = {}
            medie = {}
            [:posti_letto_totali, :posti_letto_covid_attivati, :posti_letto_covid_occupati, :posti_letto_covid_occupati_vaccinati, :posti_letto_covid_disponibili, :occupazione_posti_letto_covid, :ricoveri_covid_24h, :ricoveri_covid_24h_vaccinati, :trasferimenti_dimissioni_covid_24h, :decessi_covid_24h, :posti_letto_no_covid_attivati, :posti_letto_no_covid_occupati, :posti_letto_no_covid_disponibili, :occupazione_posti_letto_no_covid, :ricoveri_no_covid_24h, :trasferimenti_dimissioni_no_covid_24h, :decessi_no_covid_24h].each do |column|
                rispondenza.merge!(column => DailyReport.where(report_date: report_date).where("#{column} is not null").count)
                percentuale_rispondenza.merge!(column => ((rispondenza[column].to_f/strutture_attive.to_f)*100).round(1))
                somme.merge!(column => data.sum(column))
                medie.merge!(column => ((somme[column].to_f/rispondenza[column].to_f).round(2)))
                
              end

            rispondenza_tsi = {}
            percentuale_rispondenza_tsi = {}
            # [:covid_posti_tsi_totali, :no_covid_posti_tsi_totali].each do |column|
            #   rispondenza_tsi.merge!(column => DailyReport.where(report_date: report_date).where("#{column} > 0").count)
              
            # end
            # [:covid_posti_tsi_totali].each do |column|
            #   somme.merge!(column => data.sum(column))
            #   medie.merge!(column => ((somme[column].to_f/DailyReport.where(report_date: report_date).where("#{column} > 0").count.to_f).round(2)))
            # end
            
            # [:covid_posti_tsi_liberi].each do |column|
            #   somme.merge!(column => data.sum(column))
            #   medie.merge!(column => ((somme[column].to_f/rispondenza_tsi[:covid_posti_tsi_totali].to_f).round(2)))
            # end
            # [:no_covid_posti_tsi_totali].each do |column|
            #   somme.merge!(column => data.sum(column))
            #   medie.merge!(column => ((somme[column].to_f/DailyReport.where(report_date: report_date).where("#{column} > 0").count.to_f).round(2)))
            # end
            # [:no_covid_posti_tsi_liberi].each do |column|
            #   somme.merge!(column => data.sum(column))
            #   medie.merge!(column => ((somme[column].to_f/rispondenza_tsi[:no_covid_posti_tsi_totali].to_f).round(2)))
            # end
            # [:tsi_posti_totali, :tsi_posti_totali_occupati].each do |column|
            #   somme.merge!(column => data.sum(column))
            #   medie.merge!(column => ((somme[column].to_f/rispondenza_tsi[:no_covid_posti_tsi_totali].to_f).round(2)))
            # end
            #medie.merge!(tsi_indice_occupazione: ((somme[:tsi_posti_totali_occupati].to_f/somme[:tsi_posti_totali].to_f)*100).round(2))
            #percentuale_rispondenza_tsi.merge!(covid_posti_tsi_totali: ((rispondenza_tsi[:covid_posti_tsi_totali].to_f/Hospital::TSI_COVID_ATTIVE.to_f)*100).round(1))
            #percentuale_rispondenza_tsi.merge!(no_covid_posti_tsi_totali: ((rispondenza_tsi[:no_covid_posti_tsi_totali].to_f/Hospital::TSI_NO_COVID_ATTIVE.to_f)*100).round(1))
            spazi =  []
            ws.add_row(["","NUMERO Strutture attive", spazi, [strutture_attive]*8, Hospital::TSI_COVID_ATTIVE, "", Hospital::TSI_NO_COVID_ATTIVE].flatten)
            ws.add_row(["","CONTA Strutture rispondenti", spazi, rispondenza.values, rispondenza_tsi[:covid_posti_tsi_totali], "", rispondenza_tsi[:no_covid_posti_tsi_totali]].flatten)
            ws.add_row(["","PERCENTUALE Strutture rispondenti", spazi, percentuale_rispondenza.values].flatten)
            ws.add_row(["","SOMMA Parametri", spazi, somme.values].flatten)
            ws.add_row(["","MEDIA Parametri", spazi, medie.values].flatten)
            ws.add_row([""])
            ws.add_row(["Strutture non rispondenti"])
            Hospital.where.not(id:  DailyReport.where(report_date: report_date).joins(:hospital).map(&:hospital_id)).each do |hospital|
              ws.add_row(["", hospital.full_name])
            end
            file_name = "daily_report_#{report_date.to_date}"
          end
          
        when 'weekly'
          weekly_data =  DailyReport.all.to_a.group_by_week{|r| r.report_date}
          
          aggregate_data = []
          weekly_data.each do |week|
            wd = {data: week.first, type: 'week'}
            daily_data = DailyReport.where(report_date: week.first..week.first+7.days).to_a.group_by_day{|r| r.report_date}
            daily_data.each do |day|
              dd = {data: day.first, type: 'day'}
              DailyReport.content_columns.each do |col|
                next if col.type == :datetime or col.type == :date or %w("occupazione_posti_letto_covid" "occupazione_posti_letto_no_covid").include? col.name
                aggregate_data << dd.merge!(I18n.t("activerecord.attributes.daily_report.#{col.name}") => day.last.sum{|d| d.send(col.name.to_sym).to_i})
              end
              occupazione_posti_letto_covid = ((dd[I18n.t("activerecord.attributes.daily_report.posti_letto_covid_occupati")].to_f / dd[I18n.t("activerecord.attributes.daily_report.posti_letto_covid_attivati")])*100).round(2)
              aggregate_data << dd.merge!(I18n.t("activerecord.attributes.daily_report.occupazione_posti_letto_covid") => occupazione_posti_letto_covid)
              occupazione_posti_letto_no_covid = ((dd[I18n.t("activerecord.attributes.daily_report.posti_letto_no_covid_occupati")].to_f / dd[I18n.t("activerecord.attributes.daily_report.posti_letto_no_covid_attivati")])*100).round(2)
              aggregate_data << dd.merge!(I18n.t("activerecord.attributes.daily_report.occupazione_posti_letto_no_covid") => occupazione_posti_letto_no_covid)
             
            end
            dd = {data: week.first}
            DailyReport.content_columns.each do |col|
              next if col.type == :datetime or col.type == :date or  %w("occupazione_posti_letto_covid" "occupazione_posti_letto_no_covid").include? col.name
              aggregate_data << wd.merge!(I18n.t("activerecord.attributes.daily_report.#{col.name}") => week.last.sum{|w| w.send(col.name.to_sym).to_i})
            end
            occupazione_posti_letto_covid = ((wd[I18n.t("activerecord.attributes.daily_report.posti_letto_covid_occupati")].to_f / wd[I18n.t("activerecord.attributes.daily_report.posti_letto_covid_attivati")])*100).round(2)
            aggregate_data << wd.merge!(I18n.t("activerecord.attributes.daily_report.occupazione_posti_letto_covid") => occupazione_posti_letto_covid)
            occupazione_posti_letto_no_covid = ((dd[I18n.t("activerecord.attributes.daily_report.posti_letto_no_covid_occupati")].to_f / wd[I18n.t("activerecord.attributes.daily_report.posti_letto_no_covid_attivati")])*100).round(2)
            aggregate_data << wd.merge!(I18n.t("activerecord.attributes.daily_report.occupazione_posti_letto_no_covid") => occupazione_posti_letto_no_covid)
          end
          aggregate_data.uniq!
          xlsx_package = Axlsx::Package.new
          wb = xlsx_package.workbook
          wb.add_worksheet do |sheet|
            sheet.add_row(aggregate_data.first.keys)
            aggregate_data.each do |week|
              sheet.add_row(week.values)
            end
            
            
          end
          file_name = "weekly_#{Time.now.to_date}"
        end
        begin 
          #temp = Tempfile.new("daily_report.xlsx") 
          #xlsx_package.serialize temp.path
          #send_file temp.path, :filename => "daily_report.xlsx", :type => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          send_data xlsx_package.to_stream.read, :filename => "#{file_name}.xlsx", :type=> "application/vnd.openxmlformates-officedocument.spreadsheetml.sheet"
        ensure
          # temp.close 
          # temp.unlink
        end
      }
    end

  end

  def index
    respond_to do |format|
      format.html { 
        @daily_reports = DailyReportDatatable.new
        render :index 
      }
    end
  end

  # GET /daily_reports/1
  # GET /daily_reports/1.json
  def show
  end

  # GET /daily_reports/new
  def new
    redirect_to action: :index unless current_user.write_access
    @daily_report = DailyReport.new
  end

  # GET /daily_reports/1/edit
  def edit
    redirect_to action: :index unless (current_user.hospital_ids.include? @daily_report.hospital_id and current_user.write_access)
  end

  # POST /daily_reports
  # POST /daily_reports.json
  def create
    @daily_report = DailyReport.new(daily_report_params)

    respond_to do |format|
      if @daily_report.save
        format.html { redirect_to @daily_report, notice: 'Daily report was successfully created.' }
        format.json { render :show, status: :created, location: @daily_report }
      else
        format.html { render :new }
        format.json { render json: @daily_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /daily_reports/1
  # PATCH/PUT /daily_reports/1.json
  def update
    respond_to do |format|
      if @daily_report.update(daily_report_params)
        format.html { redirect_to @daily_report, notice: 'Daily report was successfully updated.' }
        format.json { render :show, status: :ok, location: @daily_report }
      else
        format.html { render :edit }
        format.json { render json: @daily_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /daily_reports/1
  # DELETE /daily_reports/1.json
  def destroy
    @daily_report.destroy
    respond_to do |format|
      format.html { redirect_to daily_reports_url, notice: 'Daily report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_daily_report
      @daily_report = DailyReport.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def daily_report_params
      params[:daily_report].permit! rescue nil
    end

    def authorize!(a,b)
      true
    end
end
